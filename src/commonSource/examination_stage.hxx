//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  January 2018
// Copyright Information:
//      Copyright (C) 1996-2018 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------
#ifndef EXAMINATIONSTAGE_HH
#define EXAMINATIONSTAGE_HH

#include "errorwarning.hxx"

class Name;

/** @class ExaminationStage
   It keeps information about Examination Stage. It is used by 
   class 'ErrorWarning' and Translator main program.
*/
class ExaminationStage
{
public:
/** called from main program before Examination Stage starts
*/
	static void start();

/** called from main program after Examination Stage stopped
*/ 
	static void stop();
	
/** updates the overall Examination Stage status based on
    the message label
*/
	static int updExamStatus(const Name& label);

/**  true when Examination Stage is in progress
*/	
	static bool inProgress();
	
/** returns the overall status of examination
*/
	static Name examStatus();  
	
/** converts the overall status of examination to enum ExitStatus type
*/
	static ExitStatus convStatusToExitStatus(); 

private:
	static bool _inProgress;  //  TRUE when Examination Stage is in progress
	                          
	static Name _status;  // overall Examination Stage status
};

#endif
