// swstay_in_state.cxx: implementation of the SWStay_in_State class.
//
//                                                B. Franek
//                                               October 2015
// Copyright Information:
//      Copyright (C) 1999/2015 CCLRC. All Rights Reserved.
//////////////////////////////////////////////////////////////////////

#include "swstay_in_state.hxx"

SWStay_in_State::SWStay_in_State( char lines[][MAXRECL], int &noLines)
{
// do
// &NULL
// 0
// &THIS_OBJECT
	Name temp = lines[0];  // to keep the compiler happy
	noLines = 4;
	return;
}

SWStay_in_State::~SWStay_in_State() { return; }

//----------------------------------------------------------------------
bool SWStay_in_State::stay_in_state() { return true;}

Name SWStay_in_State::outShort() { return "stay_in_state";}




