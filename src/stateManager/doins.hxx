//-------------------------  Class   D o I n s  -------------------------------
#ifndef DOINS_HH
#define DOINS_HH

#include "parameters.hxx"
#include "instruction.hxx"
#include "parms.hxx"
#include "whenresponse.hxx"
#include "argname.hxx"
class SMIObject;
class State;
class Action;

//                                                  Date :  5-May-1996
//                                                  Author : Boda Franek
// Copyright Information:
//      Copyright (C) 1996-2001 CCLRC. All Rights Reserved.
//---------------------------------------------------------------------------


class DoIns : public Instruction, public WhenResponse {
public:
  DoIns
 ( char lines[][MAXRECL], int lev,int *no_lines, 
  SMIObject *pobj=0, State *pstat=0, Action* act=0);
  void whatAreYou();
  
  Name outShort();  // produces a short line describing DO
  
  int execute( Name& endState );   // 0-normal,  1-terminated action, 2-suspended

  void executeHp();
	
/** returns action name
*/
	Name actionName() const;

/**  overriding the method in 'WhenResponse'
*/	
	void executeResponse();
	
	bool hasArgs();	
	
	void setCurrentArgs(const NameVector& currArgs);
	
private:
/**
 will create a parameter string for dispatching by updating the generalized values of the other object action parameters by specific current values of local parameters where necessary. 
If something goes wrong, it does not return...fatal error
@param   parString
*/
	void createOutgoingParameters(Name& parString);
	
//--------------- D A T A ------------------------------
  int _level;   //for diag. printing
  
  Name _actionNm, _objectNm;
/**
 DO intruction can take the name of the object on which it acts (_objectNm)
 as an argument. The parent Instruction Block may be supplying
 several different arguments and which argument to use has to be
 indicated. The following data item is 'generalised' object name. Its class
 contains the original object name string (that could be of the form $(argn))
 and on request it provides the actual name of the object to use. While doing
 this, it takes into account the current argument list supplied by
 enapsulating instruction block.  
*/  
	ArgName _objectNm_gen;   // generalised object name
//
  SMIObject *_pParentObject;
  
  State* _pParentState;

  Action* _pParentAction;
//
  int _numOfPara;
  Parms _doInsParameters;
};

#endif
