//-----------------------------  WaitForIns  Class ------- wait_for instruction -------
#include "smixx_common.hxx"
#include <stdio.h>
#include <assert.h>

#include "parameters.hxx"
#include "ut_sm.hxx"
//---------------------- Externals ----------------------
#include "objectregistrar.hxx"
#include "registrar.hxx"
   extern ObjectRegistrar allSMIObjects;
   extern Registrar allSMIObjectSets;
#include "resumehandler.hxx"
   extern ResumeHandler resumeHandler;
//-------------------------------------------------------
#include "instruction_return_status.hxx"
#include "ptrvector.hxx"
#include "waitforins.hxx"
#include "smiobject.hxx"
#include "state.hxx"
#include "alarm.hxx"
#include "options.hxx"
//
//                                                        B. Franek
//                                                        February 2015
// Copyright Information:
//      Copyright (C) 1996-2015 CCLRC. All Rights Reserved.
//---------------------------------------------------------------------------
//
//------------------------------ WaitForIns -------------------------------------

WaitForIns::WaitForIns
	( char lines[][MAXRECL], int lev, int& no_lines
	, SMIObject *pobj, State *pstat, Action* pact)
{
//-------------------------------------------------------------------------
// Input :
// lines[0] ....... the first line after the 'wait_for'
// lev    ....... the block level

// Output :
// no_lines ....... number of lines in 'wait' (not counting the first 'wait')
//----------------------------------------------------------------------------

//cout << endl << " WAIT_FOR constructor started " << endl;

	_objName = pobj->name();
	_pParentObject = pobj;
   
   	_pParentState = pstat;
	
	_pParentAction = pact;

//   cout << " WAIT_FOR belongs to object : " << _objName << "\n";

	_level = lev;

	_suspend_flag = 0;
	
//------------------- Loop over waitfor lines untill endwaitfor is found ----------

	int lin = 0;
	int whenInx = 0;

	for (;;lin++)
	{
//    cout << lin << ": |" << lines[lin] << "|\n";
		if (!strcmp(lines[lin],"endwaitfor"))
		{
			no_lines = lin+1;
			break;
		}
		else if (!strcmp(lines[lin],"*WHEN"))
		{
			int no_whlines;
			_whens += new When
			(&lines[lin],&no_whlines,_pParentObject,_pParentState,whenInx);
			whenInx++;
			lin = lin + no_whlines - 1;
		}
		else {
			cout << "Strange sequence " << lines[lin] << " encountered";
			cout << endl;
			Alarm::message("FATAL",_pParentObject->name(),
			" initialisation of WAIT_FOR instruction");
		}
	}

// get ref objects and sets

	int noWhens = _whens.length();
	
	for ( int iw=0; iw<noWhens; iw++ )
	{
		When* pWhen = static_cast<When*>(_whens[iw]);
		NameVector wRefObjects, wRefObjectSets; 
		pWhen->getDirectlyRefObjects(wRefObjects);
		pWhen->getRefObjectSets(wRefObjectSets);
		_refObjects.exclusiveUpdate(wRefObjects);
		_refObjectSets.exclusiveUpdate(wRefObjectSets);
	}
	
	return ;
}
//==================================================================
void WaitForIns::whatAreYou()
{
cout << " WaitForIns::whatAreYou()    not implemented yet" << endl; return;
	indent(2*_level+5);
	
	cout << "wait ( ";
	
	int io, numobj, numobjset; 
	
	numobj = _refObjects.length();
	numobjset = _refObjectSets.length();
	
	for ( io=0; io<numobj; io++)
	{
		if (io>0) {cout << ", ";}
		cout << _refObjects[io].getString() ;
	}
	

	if (numobjset ==0) {cout << " )" << endl; return;}
	else {cout << ", ";}

	for ( io=0; io<numobjset; io++)
	{
		if (io>0) {cout << ", ";}
		cout << "all_in " << _refObjectSets[io].getString() ;
	}
	
	 cout << " )" << endl;
	return;
}
//========================================================================
int WaitForIns::execute(Name& endState)
{
	int dbg; Options::iValue("d",dbg);

	endState = "not changed";

//debug beg
if ( dbg > 5 )
{
	cout << endl << " start================= WaitForIns::execute =========== " << endl;
	cout  << "   WAIT_FOR instruction(" << this << " of " << _objName << endl;
	cout << "  _suspend_flag = " << _suspend_flag << endl;
}
//debug end

	if ( _suspend_flag == 0 )
	{ // This is a fresh unsuspended WAIT_FOR
		_refObjectsCurrent.removeAll();
		
		getCurrentRefObjects();  //This obviously depends on the contents of Sets

//cout << endl << " Referenced objects at the start: " << endl;
//_refObjectsCurrent.out(" ");

// ********** execute whens ***********
		Name result = executeWhens();
		 // either 'state name', '&CONTINUE' or '&FALSE'

		if( result == "&FALSE" )
		{  // no when could be executed
			_pParentObject->setPointerToSuspendedWAIT_FOR(this);
			informServersOfSuspension(); // this is not necessary in case of WAIT. Locks will do it
			resumeHandler.registerSuspendedObject(_pParentObject);
			_suspend_flag = 1;
//debug beg
if ( dbg > 5 )
{
	cout << " WAIT_FOR suspended" << endl;
	cout << " return================= WaitForIns::execute =========== "
	 << endl << endl;
}
//debug end
			return instructionSuspended; 
		}
		else
		{ 
			if ( result == "&CONTINUE" )
			{
if ( dbg > 5 )
{
	cout << " WAIT_FOR terminated. Execution continues with the next instruction" << endl;
	cout << " return================= WaitIns::execute =========== "
	 << endl << endl;
}			
				return normal;
			}
			endState = result;
//debug beg
if ( dbg > 5 )
{
	cout << " WAIT_FOR terminated  endState : " << endState << endl;
	cout << " return================= WaitIns::execute =========== "
	 << endl << endl;
} 
			return instructionFinishesTerminatingAction;
		}	
	}
	else
	{  // this is suspended WAIT_FOR that was now released

		_suspend_flag = 0;
		getCurrentRefObjects();
		informServersOfSuspensionEnd();
		
		if ( _resultOfLastExecuteWhens == "&CONTINUE" )
		{
if ( dbg > 5 )
{
	cout << " WAIT_FOR terminated. Execution continues with the next instruction" << endl;
	cout << " return================= WaitIns::execute =========== "
	 << endl << endl;
}			
			return normal;
		}
		endState = _resultOfLastExecuteWhens;
//debug beg
if ( dbg > 5 )
{
	cout << " WAIT_FOR terminated  endState : " << endState << endl;
	cout << " return================= WaitIns::execute =========== "
	 << endl << endl;
} 
		return instructionFinishesTerminatingAction;

	}	
}
//==========================================================================
Name WaitForIns::executeWhens()
{

	Name result;

	int numWhens = _whens.length();
	When* pWhen;

//cout << endl << " Starting WaitForIns::executeWhens()" << endl;
	
	for ( int iw = 0; iw<numWhens; iw++ )
	{
		pWhen = static_cast<When*>(_whens[iw]);
		
		if (!pWhen->executeWhen()) { continue;}
		// WHEN condition is true
		result = pWhen->nextMove();
		_resultOfLastExecuteWhens = result;
		return result;
	}
	
	return "&FALSE"; // no WHEN had TRUE condition
}		
//=========================================================================
void WaitForIns::getCurrentRefObjects()
{
//cout << " WaitForIns::getCurrentRefObjects  called " << endl;

	_refObjectsCurrent.removeAll();
	
	When* pWhen; NameList condCurrRefObjects;
		
	int numWhens = _whens.length();
	
	int iw;
	
	for ( iw=0; iw<numWhens; iw++ ) // loop over whens
	{
		pWhen = static_cast<When*>(_whens[iw]);
		pWhen->getCurrentRefObjects(condCurrRefObjects);
		_refObjectsCurrent.add(condCurrRefObjects);
	}
	
	return;
}
//=========================================================================
void WaitForIns::informServersOfSuspension()
{
// First all the objects that are referenced either explicitly or implicitly
// through sets.
	Name refObjName;
	SMIObject* pSMIObj;
	
	_refObjectsCurrent.reset();
	while (_refObjectsCurrent.nextItem(refObjName) )
	{
		pSMIObj = allSMIObjects.gimePointer(refObjName);
		pSMIObj->youHaveClientWF(_objName);
		
	}
	
// secondly all the referenced Sets
	Name refSetName;
	SMIObjectSet* pSMISet;
	
	int noSets = _refObjectSets.length();
	for ( int i=0; i<noSets; i++ )
	{
		refSetName = _refObjectSets[i];
		pSMISet =
		    (SMIObjectSet*)allSMIObjectSets.gimePointer(refSetName);
		pSMISet->youHaveClientWF(_objName);
	}


	return;
}
//=========================================================================
void WaitForIns::informServersOfSuspensionEnd()
{
	Name refObjName;
	SMIObject* pSMIObj;
	
	_refObjectsCurrent.reset();
	while (_refObjectsCurrent.nextItem(refObjName) )
	{
		pSMIObj = allSMIObjects.gimePointer(refObjName);
		pSMIObj->unregisterClientWF(_objName);
		
	}
	
// secondly all the referenced Sets
	Name refSetName;
	SMIObjectSet* pSMISet;
	
	int noSets = _refObjectSets.length();
	for ( int i=0; i<noSets; i++ )
	{
		refSetName = _refObjectSets[i];
		pSMISet =
		    (SMIObjectSet*)allSMIObjectSets.gimePointer(refSetName);
		pSMISet->unregisterClientWF(_objName);
	}

	return;
}
//=============================================== BF  March 2015  =========
int WaitForIns::reportingChangedSets ()
{
	NameList refObjectsPrevious;
	
	NameList objectsRemoved, objectsAdded;
	
	Name prevObject, currObject;
	

	if ( _suspend_flag != 1 )
	{
		cout << "  *** Internal error : WAIT is not suspended" <<
		endl;
		Alarm::message("FATAL",_pParentObject->name(),
		" Class WaitForIns  method  reportingChangedSets ... internal error");
	}

	refObjectsPrevious =_refObjectsCurrent;
	
	getCurrentRefObjects();
	
	refObjectsPrevious.reset();
	while ( refObjectsPrevious.nextItem(prevObject) )
	{
		if ( _refObjectsCurrent.isPresent(prevObject) ) {}
		else
		{
			objectsRemoved.add(prevObject);
		}
	}
	
	_refObjectsCurrent.reset();
	while ( _refObjectsCurrent.nextItem(currObject) )
	{
		if ( refObjectsPrevious.isPresent(currObject) ) {}
		else
		{
			objectsAdded.add(currObject);
		}
	}
	
	Name objName;
	SMIObject* pSMIObj;
	
	objectsRemoved.reset();
	while ( objectsRemoved.nextItem(objName) )
	{
		pSMIObj = allSMIObjects.gimePointer(objName);
		pSMIObj->unregisterClientWF(_objName);
	
	}
	
	objectsAdded.reset();
	while ( objectsAdded.nextItem(objName) )
	{
		pSMIObj = allSMIObjects.gimePointer(objName);
		pSMIObj->youHaveClientWF(_objName);
	
	}
	
	
	if ( areYouReadyToResume() ) { return 1; }

	return 0;
}
//-------------------------------------------------------------------------
bool WaitForIns::areYouReadyToResume()
{
	Name result = executeWhens();
	
	if ( result == "&FALSE" ) { return false; }
	
	return true;
	
}
