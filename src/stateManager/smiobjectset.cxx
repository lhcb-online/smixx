//-----------------------------------------------------------------------
//                         SMIObjectSet  Class
// $Id
//                                                 B. Franek
//                                                  03-November-2009
// Copyright Information:
//      Copyright (C) 1999-2009 CCLRC. All Rights Reserved.
//-----------------------------------------------------------------------
//
#include <stdlib.h>
#include <stdio.h>
#include "smiobjectset.hxx"
#include "smiobject.hxx"
#include "alarm.hxx"
#include "options.hxx"
//===========================================================================
SMIObjectSet::SMIObjectSet()
	: _name(), _objectList(), _clientWhens()
{
	return;
}
//===============================================================================
int SMIObjectSet::numOfObjects() const
{ 
	int num = _objectList.numOfEntries();
	return num;
}

//============================ reset ========================================
void SMIObjectSet::reset() {
	_objectList.reset();
	return;	
}
//============================   name   =====================================

Name SMIObjectSet::name () const {
return _name;
}
//=========================== nextObject ====================================
int SMIObjectSet::nextObject(Name& name) {
        NmdPtnr tmpItem;
	
	int flg = _objectList.nextItem(tmpItem);
	name = tmpItem.name();
	return flg;
}
//=============================================================================
int SMIObjectSet::nextObject(Name& name, SMIObject*& ptnr)  {
	NmdPtnr tmpItem;
	
	int flg = _objectList.nextItem(tmpItem);
	name = tmpItem.name();
	ptnr = static_cast<SMIObject*>(tmpItem.pointer());

	return flg;
}

//------------------------------------------ BF  9-Apr-2008 -------------------
int SMIObjectSet::addClientWhen(const char* whenObjName, const char* whenStateName, int whenInx)
{
	return _clientWhens.markWhen(whenObjName, whenStateName, whenInx);
}
//------------------------------------------  Bf  11-Apr-2008  --------------
int SMIObjectSet::printClientWhens() const
{
	int numClientObjects =_clientWhens.numObjects();
	if ( numClientObjects == 0 )
	{
		cout << " No clients " << endl;
		return 1;
	}
	
	_clientWhens.out(" ");
	return 1;
}
//------------------------------------------  BF  09-May-2008  ----------------
int SMIObjectSet::informObjectsAboutMembership() 
{

//cout << endl 
//<< "start================== SMIObjectSet::informObjectsAboutMembership ========"
// << endl;
//cout << "  Set  " << _name << "   Objects : " << endl;
//Name temp(" "); _objectList.out(temp);
 	
	int numOfObjects = _objectList.numOfEntries();
	if ( numOfObjects <= 0 ) { return 1;}

	NmdPtnr currentSet(_name,this);
	NmdPtnr tmpItem;
	SMIObject* pObj;

/* Loop over the SMI objects and ads the current set to the list */
	_objectList.reset();
	while ( _objectList.nextItem(tmpItem) )
	{
		pObj = static_cast<SMIObject*>(tmpItem.pointer());
		pObj->_setsIBelongTo.add(currentSet);
		
//cout << " Object : " << pObj->name() << " is now member of : " << endl;
//Name temp(" "); pObj->_setsIBelongTo.out(temp);

	}
	return 1;
}
//------------------------------------------ BF  14-May-2008  -----------------
const ClientWhens& SMIObjectSet::gimeClientWhensRef() const
{
	return _clientWhens;
}
//=============================================================================
void SMIObjectSet::gimeObjectList(SMISetContainer& objectList) const
{
	objectList = _objectList;
	
	return;
}
//============================================================================
void SMIObjectSet::mergeInObjectList(SMISetContainer& outObjectList)
{
	NmdPtnr listItem;
	
	_objectList.reset();
	
	while (_objectList.nextItem(listItem))
	{
		outObjectList.add(listItem);
	}
	
	return;
}
//------------------------------------------------------------------------------
void SMIObjectSet::youHaveClientWF(Name& clientName)
{
	int dbg; Options::iValue("d",dbg);

	int iflg = _clientObjectsWF.add(clientName);
	
	if (!iflg) 
	{
		cout << endl << " **** FATAL ERROR *****" << endl
		<< "    SMIObjectSet::youHaveClientWF(Name& clientName) " << endl  
		<< " Object Set : " << _name
		 << " adding already existing client Object : " << clientName << endl;
		Alarm::message("FATAL",clientName,
		  " adding/removing into/from SET error"); 
	}
if ( dbg>5 )
{
cout << endl
<< " Object Set: " << _name
		 << " adding client Object : " << clientName << endl;
}
	return;
}
//------------------------------------------------------------------------------
void SMIObjectSet::unregisterClientWF(Name& clientName)
{
	int dbg; Options::iValue("d",dbg);

	int iflg = _clientObjectsWF.remove(clientName);
	
	if (!iflg) 
	{
		cout << endl << " **** FATAL ERROR *****" << endl
		<< "    SMIObjectSet::unregisterClientWF(Name& clientName) " << endl  
		<< " Object Set : " << _name
		 << " removing nonexisting client Object : " << clientName << endl;
		Alarm::message("FATAL",clientName,
		  " adding/removing into/from SET error");  
	}
if ( dbg >5 )
{
cout << endl
<< " Object Set : " << _name
	 << " removed client Object : " << clientName << endl;
}
	return;
}

//-----------------------------------------------------------------------------
NameList& SMIObjectSet::gimeWFClients()
{
	return _clientObjectsWF;
}
//----------------------------------------------------------------------------
bool SMIObjectSet::isObjectClient(const Name objName)
{
	return _clientObjectsWF.isPresent(objName);
}
