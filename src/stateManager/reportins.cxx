//-------------------------  Class   ReportIns  -------------------------------
//                                                  Date :    August 2017
//                                                  Author : Boda Franek
// Copyright Information:
//      Copyright (C) 1996-2017 CCLRC. All Rights Reserved.
//---------------------------------------------------------------------------

#include "ut_sm.hxx"
#include "assert.h"
#include "reportins.hxx"
#include "alarm.hxx"
#include "report.hxx"
#include "smiobject.hxx"
#include "state.hxx"
#include "action.hxx"

ReportIns::ReportIns
           ( char lines[][MAXRECL], int lev, int& no_lines,
            SMIObject* pobj, State *pstat, Action* pact)
{
//-------------------------------------------------------------------------
// Input :
// lines[0] ....... the first line after the 'report'
// lev    ....... the block level

// Output :
// no_lines ....... number of lines in 'report' (not counting the first //                         'report')
//----------------------------------------------------------------------------

	_level = lev;
	
	_pParentObject = pobj;
	_pParentState = pstat;
	_pParentAction = pact;
	
	int il = 0;
	_severity = lines[il];
	
	il++;
	int noElem;
	sscanf(lines[il],"%d",&noElem);
	assert (noElem > 0);
	
	Name tmpElem;
	
	for (int ie=0; ie<noElem; ie++ )
	{
		il++;
		tmpElem = lines[il];
		_msgElements += tmpElem;
	}
	
	no_lines = il + 1;
	return;
}
//--------------------------------------------------------------------------
void ReportIns::whatAreYou()
{
	indent(2*_level+5);
	
	cout << "report ( " << _severity.getString() << ","
	 << (_msgElements[0]).getString() ;
	
	int ie, numElem;
	
	numElem = _msgElements.length();
	
	for ( ie=0; ie<numElem; ie++ )
	{
		cout << " + " << (_msgElements[ie]).getString() ;
	}
	
	cout << " )" << endl;
	return;
}
//--------------------------------------------------------------------------
int ReportIns::execute( Name& endState )
{
	endState = "not changed"; // the instruction does not change state
	
//  Building the message from message elements in the report instruction
//  While doing it we shall strip enclosing double quotes from string
//  values and finish with only enclosing doublequotes on the complete
//  message
//-------------------------------------

	Name message = "";	
	int ie, numElem;
	int flg;
	
	numElem = _msgElements.length();
	Name element("");
	Name parNm, parVal, parValType, result;
	
	for ( ie=0; ie<numElem; ie++ )
	{
		element = _msgElements[ie];
		if ( element[0] == '\"' )
		{  // this is a constant
			//cout << element << endl;
			flg = stripEnclosingQuotes(element);
			if (flg) { message += element; continue;}
			else
			{
			 	cout << " message element " << element
				 << " has screwed up quotes this should not happen" << endl;

			 	Alarm::message("FATAL", _pParentObject->name(),
		        	" problem with message element");
				return 0;	
			}	
		}
		// parameter name
		parNm = element;

		flg = getLocalParameter(parNm,	
		                         _pParentObject,
					 _pParentState,
					 _pParentAction,
					 parVal,parValType,result);		 
		if (flg)
		{
			//cout << parVal << "  " << result << endl;
			if ( parValType == "STRING" )
			{
				if ( stripEnclosingQuotes(parVal) )
				{ message += parVal; }
				else 
				{ 
			 		cout << " The value of parameter " << parNm
				 	<< " has screwed up quotes : " << parVal << endl
					<< " this should not happen" << endl;

			 		Alarm::message("FATAL", _pParentObject->name(),
		        		" problem with message element");
					return 0;	
				}
			}
			else { message += parVal; }
		}
		else
		{
			Alarm::message("FATAL", _pParentObject->name(),
		        " problem with message elements");

			// if getLocalParameter does not succeed, it is considered
			// fatal error and the program terminates.	
		}	
	}
	
//------------------------------------------------------------
//	cout << endl << message << endl;

// The message is successfully built, now we can send it

	Report::message(_severity,_pParentObject->name(),message);
	
	return 0;  // normal return from execute method
}

