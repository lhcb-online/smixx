//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  January 2016
// Copyright Information:
//      Copyright (C) 1996-2016 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------
#ifndef ALARM_HH
#define ALARM_HH

class Name;

class Alarm {
public:

	static void initialise();

	static void message
	   (const char* severity, const Name& object, const char* mess);
	   
// severity is INFO WARNING ERROR FATAL
	
private:

};

#endif
