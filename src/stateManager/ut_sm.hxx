//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  August 2001
// Copyright Information:
//      Copyright (C) 1996-2013 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------
#ifndef UT_SM_HH
#define UT_SM_HH


class Name;
class NameVector;
class ParOperand;
class SMIObject;
class State;
class Action;

#include "reservednames.hxx"
#include "objectregistrar.hxx"
   extern ObjectRegistrar allSMIObjects;
   
   
void gime_millis(char* millis);
void newTime(char *timeStr);

void indent(int blanks);
void print_msg(const char *msg);
void gime_date_time(char *timeStr);
void print_date_time();
void print_obj(char *obj_name);
void print_obj(const Name& obj_name);

void makeStateString(const NameVector& states, Name& stateString);

int stringParToIntPar(Name& in, Name& out);
int floatParToStringPar(Name& in, Name& out);
int floatParToIntPar(Name& in, Name& out);
int intParToFloatPar(Name& in, Name& out);
int intParToStringPar(Name& in, Name& out);

//-----------------------------------------------------------------------------
/** If the supplied string is enclosed in doublequotes, it will remove them.
   @retval <1>  doublequotes were present and removed
   @retval <0>  Something wrong with the quotes. The string is not modified.
*/
	int stripEnclosingQuotes( Name& string);	

//---------------------------------------------------------------------------
/** Finds the value of reserved name in the given context
    Used by 'getOperandValue' function and 'DoIns' class
    @retval <1> success
    @retval <0> failure
    @param  resname    name of the reserved name (_DOMAIN_, _OBJECT_  etc)
    @param  pParentObject   pointer to the executed instruction's parent object
    @param  pParentState    -dtto- fot state
    @param  pParentAction   -dtto- for action 
    @param  resval   value of the reserved name
    @param  restype  type of the reserved name. At the moment only STRING.    
*/
int getReservedNameValue
	    (Name& resname,
	     SMIObject* pParentObject, State* pParentState, Action* pParentAction,
	     Name& resval, Name& restype);
//--------------------------------------------------------------------------------
/**  Given the operand, it returns value of the associated parameter. This 
     function is used by 'SetIns' class and 'SmpCondTyp4' class during
     execution of an instruction.
    @retval <1> success
    @retval <0> failure
    @param operand
    @param  pParentObject   pointer to the executed instruction's parent object
    @param  pParentState    -dtto- fot state
    @param  pParentAction   -dtto- for action 
    @param  value   value of the operand
    @param  type    type of the operand (INT,FLOAT or STRING)
*/

int getOperandValue
   ( ParOperand& operand,
    SMIObject* pParentObject, State* pParentState, Action* pParentAction,
    Name& value, Name& type);
//--------------------------------------------------------------------------------
/** Given the name of a parameter that was declared localy either as object
    parameter or action parameter, or the name is reserved name, it returns its
    value and type.
    This method is called as part of the execution of an instruction and used by
    'getOperandValue' function and 'ReportIns' class
    @retval <1> success
    @retval <0> failure, in which case it returns the reason for it in
                          argument 'result'.
    @param  parNm   The name of the parameter
    @param  pParentObject   pointer to the executed instruction's parent object
    @param  pParentState    -dtto- fot state
    @param  pParentAction   -dtto- for action  
    @param  parVal   returned value of the parameter.
    @param  parValType   returned type of the parameter (INT,FLOAT or STRING)
    @param  result  if the call was successful, then it is set to either
                    RESERVED-NAME, or OBJECT-PARAMETER or ACTION-PARAMETER
                    if the call was unsuccessfull, then it is set to the reason
		    for failure: RESERVED-NAME-NOACCESS, or PARAMETER-NOTDECLARED
*/	
	int getLocalParameter(Name& parNm,
                              SMIObject* pParentObject,
			      State*  pParentState,
			      Action* pParentAction,
			      Name& parVal, Name& parValType, Name& result);
//------------------------------------------------------------------------------------
/** Given the name of a parameter and its object name it returns its
    value and type. Used by 'getOperandValue' function
    @retval <1> success
    @retval <0> failure, in which case it returns the reason for the failure in
                          argument 'result'.
    @param  parNm   The name of the parameter
    @param  objNm   The name of the object
    @param  parVal   returned value of the parameter.
    @param  parValType   returned type of the parameter (INT,FLOAT or STRING)
    @param  result  if the call was successful, then it is set OBJECT-PARAMETER 
                    if the call was unsuccessfull, then it is set to the reason
		    for failure: PARAMETER-NOTDECLARED
*/
	int getAnyObjParameter(Name& parNm, Name& objNm,
			    Name& parVal, Name& parValType, Name& result);
//---------------------------------------------------------------------------------			
#endif
