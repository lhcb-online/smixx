//------------------------  Class   D o I n s  -----------------------------
#include "smixx_common.hxx"
#include <stdio.h>
#include <assert.h>
#include "utilities.hxx"
#include "ut_sm.hxx"
//-------------------- Externals -----------------
#include "objectregistrar.hxx"
   extern ObjectRegistrar allSMIObjects;
   extern  Name smiDomain;
#include "registrar.hxx"
   extern Registrar allSMIObjectSets;
//-------------------------------------------------
#include "doins.hxx"
#include "smiobjectset.hxx"
#include "smiobject.hxx"
#include "parms.hxx"
#include "alarm.hxx"
#include "options.hxx"
#include "reservednames.hxx"
//                                                      Date : 5-May-1996
//                                                     Author: Boda Franek
// Copyright Information:
//      Copyright (C) 1996-2001 CCLRC. All Rights Reserved.
//---------------------------- DoIns -------------------------------------
//
DoIns::DoIns
( char lines[][MAXRECL], int lev, int *no_lines, 
  SMIObject *pobj, State *pstat, Action* pact){
   char tempstr[MAXRECL];
   int lin;
   
	if ( pstat == NULL ) {}  // will remove compiler warning
	
   _level = lev;

   _pParentObject = pobj;
   
   _pParentState = pstat;

   _pParentAction = pact;
//    cout << " do init _pParentAction : " << _pParentAction << "\n";

   sscanf(lines[1],"%s",tempstr);
   _actionNm = tempstr;

   sscanf(lines[2],"%d",&_numOfPara);

   lin = 3;

   Name paraNm, paraGenVal, paraGenType;

   if ( _numOfPara > 0) {
      // For every parameter Translator sends just its name and its generalized
      // value
      for(int ipar=0; ipar<_numOfPara; ipar++) {
         strcpy(tempstr,lines[lin]);
//         sscanf(lines[lin],"%s",tempstr);
         paraNm = tempstr;
         lin++;
	 strcpy(tempstr,lines[lin]);
//         sscanf(lines[lin],"%s",tempstr);
         paraGenVal = tempstr;

         if ( !strcmp(tempstr,"&SMIDOMAIN") ) {
            paraGenVal = "\"";
            paraGenVal += smiDomain;
            paraGenVal += "\"";
	    paraGenType = "STRING";
         }
	 else
	 { 
	 // as Translator has not supplied the generalized type, we determine it
	 // ourselves
	 	paraGenType = getGenValueType(paraGenVal);
		
		if ( paraGenType == "STRING" ||
		     paraGenType == "INT" ||
		     paraGenType == "FLOAT" ||
		     paraGenType == "NAME" )
		{}
		else
		{
			cout << " --Error: DoIns::Doins(...)" << endl
			 << " generic value " << paraGenVal 
			 << " of parameter " << paraNm 
			 << "  has illegal type " << paraGenType << endl
			 << "  Fatal error...this should not happen" << endl;
			Alarm::message("FATAL",_objectNm,
			    " Processing Do ... problem with parameters");	
		}
	}
        
        lin++;
        _doInsParameters.add_nocheck(paraNm,paraGenVal,paraGenType);
      }
   }

   sscanf(lines[lin],"%s",tempstr);
   _objectNm = tempstr;
   
	_objectNm_gen.init(tempstr);
	   
   *no_lines = lin + 1;
   return;
}



//---------------------------- whatAreYou ---------------------------------

void DoIns::whatAreYou(){
   indent(2*_level+5);

// Format: do RO_SETUP ("/RO_FILE="SMI_LES_RO_FILE) LES

   cout << "do " << _actionNm ;

   if (_numOfPara > 0 ) {
      cout << " (";
      for (int ip=0; ip<_numOfPara; ip++) {
//         cout << _para[ip] << _value[ip];
//         if ( ip<(_numOfPara-1) ){cout << ",";}
      }
      cout << ")";
   }

   cout << "  " << _objectNm  << "\n";
}

//----------------------------  execute  ---------------------------------------
//
//
int DoIns::execute( Name& endState ){

	int dbg; Options::iValue("d",dbg);


//debug beg
if ( dbg > 3 )
{
	indent(3*_level+5);
	cout << "DO instruction " << _actionNm <<  "  "  << _objectNm ;
	indent(3*_level+5);
	if (hasArgs())
	{
	cout << "(arg" << _objectNm_gen.arg() << ")  executing" << endl;
	}
	else { cout << "  executing" << endl; }
}
//debug end

   Name actionString = _actionNm;  

	if (_numOfPara > 0) 
	{  // build parameter string for dispatch and append it to action name
		Name tempParString;
		createOutgoingParameters(tempParString);
		
		actionString += tempParString;

//    cout << " Parameter string : " << tempParString << endl;
	}
//---------------------------------------------------------------------------

//   cout << " Action string : " << actionString << endl;
//   cout.flush();

	SMIObject* pSMIObj;
	char* pObjectName = _objectNm.getString();

	if (strncmp(pObjectName,"&ALL_IN_",8) == 0) {
		Name setName = &pObjectName[8];
		void* ptnv = allSMIObjectSets.gimePointer(setName);
		if (ptnv == 0) { cout << " Set " << setName
			              << "  not declared " << endl;
				 Alarm::message("FATAL",_objectNm,
				 " Processing DO ... undeclared SET");}
		SMIObjectSet* ptnSet = (SMIObjectSet*) ptnv;
		ptnSet->reset();
		Name objectName;
		while (ptnSet->nextObject(objectName)) {
			pSMIObj = allSMIObjects.gimePointer(objectName);
			if ( pSMIObj == 0 ) {
			cout << " Object " << objectName 
			<< " in set " << setName << " does not exists" << endl;
			Alarm::message("FATAL",_objectNm,
			" Processing DO ... an object missing in SET");
			}
			pSMIObj->queueAction(actionString);
		}
	}

	else if (strncmp(pObjectName,"&VAL_OF_",8) == 0) {
		Parms* pCurPars = _pParentAction->pCurrentParameters();
		Name parName = &pObjectName[8]; Name parVal; char type[10];
//		pCurPars->out(); 
		if(!pCurPars  || !pCurPars->get(parName,parVal,type)) {
			cout << " No value for parameter " << parName << endl;
		}
		if ( dbg > 2 ) {
        	    cout << " Parameter " << parName 
                    << " has value " << parVal << endl;
		}
		char* parValchr = parVal.getString(); 
		parValchr[strlen(parValchr)-1] = '\0';
		Name objNm = &parValchr[1];
		pSMIObj = allSMIObjects.gimePointer(objNm);
		if (!pSMIObj) {
			cout << " Object " << objNm 
			<< " does not exists" << endl;
		}
		else {
			pSMIObj->queueAction(actionString);
		}		
	}
	else {
		pSMIObj = allSMIObjects.gimePointer(_objectNm);
		pSMIObj->queueAction(actionString);
	}



   endState = "not changed";
   return 0;         // do instruction allways finishes
}


//-------------------------  executeHp  ---------------------------------------
//
//
void DoIns::executeHp(){

	int dbg; Options::iValue("d",dbg);
    
	Name actionString = _actionNm;
	
	if ( _numOfPara > 0 )
	{  // build parameter string for dispatch and append it to action name
		Name tempParString;
		createOutgoingParameters(tempParString);
		
		actionString += tempParString;
	}
	
if ( dbg > 7 )
{
  cout << " Action string : " << actionString << endl;
}
    	_pParentObject->queueHpAction(actionString);

	return ;
}
//------------------------- actionName --------------------------------------
Name DoIns::actionName() const
{
	return _actionNm;
}
//--------------------------------------------------------------------------
void DoIns::createOutgoingParameters(Name& parmString)
{
//         do OTHER_OBJECT_ACTION(OTHER_OBJECT_ACTION_PAR_1 = 75,...
//                                OTHER_OBJECT_ACTION_PAR_N = LOCAL_PAR,...)
//                                OTHER_OBJECT
//
//    otObject  ...  other object 
//    otObjAct ... other object action
//    otObjActPar ... other object action parameter
//
//cout << endl << endl << " Entering createOutgoingParametersNew" << endl
//<<  " Do parameters :" << endl;    _doInsParameters.out();

	int dbg; Options::iValue("d",dbg);

	Parms tmpPars; // temporary DO parameters
	int flg;

if ( dbg > 7 )
{
    cout << " Do parameters :" << endl;    _doInsParameters.out();
}
	Name otObjActParNm,           // other object action parameter name
	            genVal,           // its generized value 
		    genValType;   // its type, INT, FLOAT, STRING, NAME
	char tmptype[10];   
	
	Name parSpecVal,         // specific value dispatched in DO
	     parSpecValType,     // specific value type:   INT, FLOAT or STRING
	     result;    // not used, but space needed
	
	int numPar = _doInsParameters.numOfEntries();
	
	Name localParNm;
	
	// objective of this do is to replace the dispatch generalized values by
	// specific values and also supply the correct value type
	
	
	for ( int ipar=0; ipar<numPar; ipar++ )
	{
		_doInsParameters.get(ipar,otObjActParNm,genVal,tmptype);

		genValType = tmptype;
		
//cout << "  genVal " << genVal << "  genValType " << genValType << endl;
				
		if ( genValType == "NAME" )
		{  // value is the name of a local parameter
			localParNm = genVal;
			flg = getLocalParameter(localParNm,
			                        _pParentObject,_pParentState,_pParentAction,
						parSpecVal,parSpecValType,result);
			if (flg) {}
			else
			{
			        Alarm::message("FATAL",_objectNm,
	  			 " Processing DO ... problem with parameters");
			}			
		}
		else
		{
			parSpecVal = genVal;
			parSpecValType = genValType;
		}
		tmpPars.add(otObjActParNm,parSpecVal,parSpecValType.getString());
		
	}

	tmpPars.buildParmString(parmString);
	return; 
}
//---------------------------------------------------------------------------
void DoIns::executeResponse()
{
	executeHp();
	return;
}
//----------------------------------------------------------------------------
Name DoIns::outShort()
{
	Name temp;
	
	temp = "do ";
	temp += _actionNm;
	temp += " ";
	
	if (_numOfPara > 0) { temp += "(parameters) "; }
	
	temp += _objectNm;
	
	return temp;
}
//---------------------------------------------------------------------------
bool DoIns::hasArgs()
{      
	return _objectNm_gen.argument();
}
//--------------------------------------------------------------------------
void DoIns::setCurrentArgs(const NameVector& currArgs)
{
	ArgNameStatus_t err;

	if (hasArgs())  // this DO has to have object name  supplied as argument
	{
		_objectNm = _objectNm_gen.name(currArgs,err);
		if (err == notenoughtargs)
		{
			cout << "   FATAL error   object name has to be supplied" << endl;
			cout << "        as argument no " << _objectNm_gen.arg() << endl;
			cout << " not enough arguments supplied" << endl;
			Alarm::message("FATAL",_pParentObject->name(),
			" Processing DO ... not enough arguments supplied");
		}
		
		if (!check_name(_objectNm))
		{
			cout << "   FATAL error   object name  " << _objectNm << endl;
			cout << "           supplied as argument is not a name " << endl;
			Alarm::message("FATAL",_pParentObject->name(),
			"  Processing DO ... supplied object name not a name");
		}
	}
}
