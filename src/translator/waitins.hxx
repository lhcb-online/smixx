// waitins.hxx: interface for the WaitIns class.
//
//                                                  B. Franek
//                                                16 December 2008
//
// Copyright Information:
//      Copyright (C) 1999-2008 CCLRC. All Rights Reserved.
//////////////////////////////////////////////////////////////////////
#ifndef WAITINS_HH
#define WAITINS_HH

#include "namevector.hxx"
#include "instruction.hxx"

class WaitIns  : public Instruction 
{
public:
	WaitIns();

	virtual ~WaitIns();

	virtual void translate() ;

	void out(const Name offset) const;

	virtual void outSobj(ofstream& sobj) const;
		
	int examine();
		
	Name outString();
/**
      The first name in the 'args' array that
      matches one of the _refObjects will cause that object to be
      replaced by $(argi) where i is the position of the matching element in
      'args' array. Similarly for _refObjectSets.
*/
	void replaceArgs(const NameVector& args);

protected :	

	NameVector	_refObjects;
	NameVector 	_refObjectSets;

};

#endif 
