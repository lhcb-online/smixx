//-----------------------------------------------------------------------
//                         State  Class
//                                                 B. Franek
//                                                 28 March 1999
//-----------------------------------------------------------------------
//
#ifndef STATE_HH
#define STATE_HH
#include "name.hxx"
#include "attributeblock.hxx"
#include "registrar.hxx"
#include "smlunit.hxx"
#include "namevector.hxx"
#include "smlsectiontype.hxx"

class Action;

class  State : public SMLUnit {

	public :

		State (const Name& name, const Name& subobj);

		virtual ~State();

		virtual void translate();

		Name subObject() const;

		void out(const Name) const;

		virtual void outSobj(ofstream& sobj) const;
		
		int getAttributes(NameVector& attr) const; //returns their number
		
		bool isUndeclared() const;
		
		bool hasAction(const Name& actionNm) const;
		
		int numActions() const;
		
		Action* gimeActionPointer(const int i) const;
		
/**
  Finds pointer to action actionNm.  NULL if not found
*/
		Action* gimeActionPointer(const Name& actionNm) const;

	protected :

		Name  _subobjectName;

		AttributeBlock *_pAttributeBlock;

		Registrar _whens;

		Registrar _actions;

	private :
			
		void createCollectTranslateWhen
		        (int& istart, int& next_istart, SMLlineType_t& nextUnit);
			
		void createCollectTranslateAction
		        (int& istart, int& next_istart, SMLlineType_t& nextUnit);
		
		void processAttributeBlock
		        (int& istart, int& next_istart, sectionType_t& nextSection);
		
		void processWhens(int& istart, int& next_istart, sectionType_t& nextSection);
		
		void processActions(int& istart, int& next_istart, sectionType_t& nextSection);		

};
#endif
