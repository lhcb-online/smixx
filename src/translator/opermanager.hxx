//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  August 2014
// Copyright Information:
//      Copyright (C) 1996-2014 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------
#ifndef OPERMANAGER_HH
#define OPERMANAGER_HH

#include "name.hxx"

class SMLline;
class SMLUnit;
class ParOperand;

	enum getParValueTypeStatus_t
	{OK=1,
	 NOTDECLARED=0,
	 REMOBJNOTDECLARED=-1,
	 REMPARNOTDECLARED=-2,
	 RESNAMENOACCESS=-3};
	 


/** @class OperManager  (Operand Manager)
  This class encapsulates functions that operate and manipulate operands
  in 'ParOperand' class. As these functions are used by both classes
  'SetIns' and 'SmpCondTyp4', they were encapsulated in the present class
  to avoid using global functions or alternatively duplication of code.
  
  All methods in this class are taking as input parameters\n
  'SMLline& firstLine'  and  'SMLUnit* pParentUnit' :\n
  This is the first line and pointer to the Parent SML Unit of the operand.
  In the present context the notion of 'Parent SML Unit' is defined as
  the closest SMLUnit in the containing hierarchy moving 'upwards'.
  An operand belongs either to SET instruction (SetIns class) or to
  Simple condition type 4 (SmpCondTyp4 class)(In the following SCT4).
  Therefore:\n
  \verbatim
     When the operand belongs to SET condition, its parent unit is the SET
     instruction itself.(because SET instruction inherits from SMLUnit)

     When the operand belongs to SCT4 then the parentage is decided by
     the container of the simple condition i.e:
        If SCT4 is contained in IF instruction, then the parent is 
        the If instruction's 'If Unit Head Block'.
        If SCT4 belongs to WHEN clause, then its parent is 'When'
  \endverbatim
  
*/
class OperManager {
public:
/**  The method gets the value type of the referenced parameter and then 
     it sets the definite Operator Value Type taking into account possible
     user casting. 
   @retval <0>  success
   @retval <1>  Examination failure
   @param  firstLine  the first line of the parent SML Unit.See the explanation
                      in the present class introduction.
   @param  pParentUnit  pointer to the parent SML Unit.
   @param  operand   the operand in question  
*/
	static int  getParValueAndOperValueTypes
	           ( SMLline& firstLine, SMLUnit* pParentUnit, ParOperand& operand);

/** The method is called at examination stage of either SCT4 or SET instruction
    and checks if the two operands are of the same type. If they are, it
    returns. If they are not, then if any or both of them have the user cast it
    will throw it back at the user to sort it out, otherwise it will attempt its
    own casting.
   @retval <0>  success
   @retval <1>  Examination failure
   @param  firstLine  the first line of the parent SML Unit.See the explanation
                      in the present class introduction.
   @param  pParentUnit  pointer to the parent SML Unit.
   @param  operand1   the first operand in either in SCT4 or on the right side
                      of SET instruction.
   @param  operand2   the second operand in ...(see description of operand1)		        
*/
	static int makeOperandsSameType
	           ( SMLline& firstLine, SMLUnit* pParentUnit,
		     ParOperand& operand1, ParOperand& operand2);
private:

/**
   If operand type is VALUE, then it does nothing.
   In the two other cases it will get the value type of the referenced
   parameter
   @retval <1> {success}
   @retval <0> { parameter not found either among action or object parameters}
   @retval <-1> {the other object or its class not declared}
   @retval <-2> {the parameter not found among the other object parameters}
   @retval <-3> {reserved name is not accessible}
   @param  pParentUnit  pointer to the parent SML Unit.See the explanation
                        in the present class introduction.
   @param  operand   the operand in question  
*/
	static getParValueTypeStatus_t getParValueType( SMLUnit* pParentUnit, ParOperand& operand);

/** This method is used by 'makeOperandsSameType' method and it attemts
    casting parameter type to which the operand refers to 'castTo'.
    If this casting is legal, then it will set this cast to be the definite
    Operator Value Type. Failure is serious Examination error.
   @retval <0> cast request succeeded
   @retval <1> cast request failed
   @param  firstLine  the first line of the parent SML Unit. See
                      the explanation in the present class introduction.
   @param  pParentUnit  pointer to the parent SML Unit. See the explanation
                        in the present class introduction.
   @param  operand   the operand in question  
   @param  castTo  (INT, FLOAT or STRING) cast request.
*/
	static int attemptCasting
	           ( SMLline& firstLine, SMLUnit* pParentUnit,
		     ParOperand& operand, const char castTo[]);
};

#endif
