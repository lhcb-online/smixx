// doins.hxx: interface for the DoIns class.
//
//                                                  B. Franek
//                                                30 September 1999
//
//////////////////////////////////////////////////////////////////////
#ifndef DOINS_HH
#define DOINS_HH

#include "name.hxx"
#include "parms.hxx"
#include "instruction.hxx"
#include "whenresponse.hxx"

class Action;
class SMIObject;
class State;

class DoIns  : public Instruction, public WhenResponse
{
public:
	DoIns();	
/**
     Constructor when object name is not supplied from SML code
     like for example in WHEN
*/
	DoIns(const Name& objectName);

	virtual ~DoIns();

	virtual void translate() ;

	void out(const Name offset) const;

	virtual void outSobj(ofstream& sobj) const;
		
	int examine();
			
  	Name outString();
/**
   The first name in the 'args' array that matches _objectNm will cause
   _objectNm to be replaced by $(argi) where i is the position of the matching
   element in 'args' array
*/	
	void replaceArgs(const NameVector& args);

protected :

	int examineAction
	   ( SMIObject* pParentObject, State* pParentState, Action* pParentAction,
	    SMIObject* pTargetObject);

	int examineParameters
	   ( SMIObject* pParentObject, State* pParentState, Action* pParentAction,
	    SMIObject* pTargetObject);
	   
	int checkCompatibility
	    (const Name& targetObjectStateName, Action* pTargetAction);

//-------------   Data  ----------	
	Name _actionNm, _objectNm;

	Parms _parameters;

};

#endif 
