// when.hxx: interface for the When class.
//
//                                                  B. Franek
//                                                 23 September 1999
//
//////////////////////////////////////////////////////////////////////
#ifndef WHEN_HH
#define WHEN_HH

#include "name.hxx"
#include "smlunit.hxx"
#include "condition.hxx"

class Action;
class DoIns;
class WhenResponse;
class WFWcontinue;

class When  : public SMLUnit 
{
public:
	When();

        When(int ident, bool belongsToWaitFor = false);
	
	virtual ~When();

	virtual void translate();

	void out(const Name offset) const;

	virtual void outSobj(ofstream& sobj) const;
	
	SMLlineVector* getEndStateActionCodePointer() const;

	int examine();
		
  	Name outString();	



protected :
		
	int _ident;

	Condition _condition;
	
	WhenResponse* _pResponse;
		
	bool _belongsToWaitFor;
};

#endif 
