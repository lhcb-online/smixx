//-----------------------------------------------------------------------
//                         State  Class
//                                                 B. Franek
//                                                 28 March 1999
// Copyright Information:
//      Copyright (C) 1996-2002 CCLRC. All Rights Reserved.
//-----------------------------------------------------------------------
//
#include <stdlib.h>
#include <assert.h>
#include "when.hxx"
#include "action.hxx"
#include "state.hxx"
#include "smlline.hxx"
#include "errorwarning.hxx"

//--------------------------- Constructors -------------------------------

State::State (const Name& name, const Name& subobj) 
	:SMLUnit("State",50,name),
	 _subobjectName(subobj),
	 _pAttributeBlock(NULL),
	 _whens(),
	 _actions()
{
	return;
}


State::~State() {
	delete _pSMLcode;
}

//-----------------------------------------------------------------------------
void State::translate() {
// The code of State unit is in _pSMLcode begining with line zero.
// It consists of 3 sections:
//
//     1) Attribute Block. This consists of all the lines at the beginning
//        of State unit starting with line zero plus all the lines specifying
//        the State attributes. This is the only section that needs to be present.
//
//     2) WHEN section  with all WHEN declarations
//
//     3) ACTION section with all the ACTIONS.

	int istart;   // first line number (in _pSMLcode vector) of the section
	              // to be processed.
	sectionType_t nextSection;  // this indicates what type of section is following
	                  // the current (processed) section.
	int next_istart; // first line number (in _pSMLcode vector) of
	                      // the section following the processed section..
		
	istart = 0;
	processAttributeBlock(istart,next_istart,nextSection);
	if ( nextSection == SECTION_NULL ) { return;} // nothing else is following
	
	if ( nextSection == SECTION_WHENS )
	{
		istart = next_istart;
		processWhens(istart,next_istart,nextSection);
		if ( nextSection == SECTION_NULL ) { return;}
	}
		
	if ( nextSection == SECTION_ACTIONS )
	{
		istart = next_istart;
		processActions(istart,next_istart,nextSection);
		if ( nextSection == SECTION_NULL ) 
		{ // end of successfull translation
//			Name temp(" "); out(temp); 
			return;
		}
	}
	
	ErrorWarning::printHead("ERROR",(*_pSMLcode)[next_istart]);
	cout << " missplaced section" << endl;
	throw FATAL;
	
	return;

}
//------------------------------------------------------- BF April 2000 ---------
Name State::subObject() const
{
	return _subobjectName;
}
//-------------------------------------------------------  BF April 2000  -------
void State::out(const Name offset) const
{
cout << " Do not use State::out(...)     It needs carefull revamping   BF" << endl;
return;
	SMLUnit::out(offset);
	cout << offset.getString() << "  ................................." << endl 
		<< offset.getString() << "Subobject : " << _subobjectName << endl;
	_pAttributeBlock->out(offset);

	cout << "  WHENS " << endl;

	int nw = _whens.length();

	for (int iw=0; iw < nw; iw++) {
		void* ptnvoid = _whens.gimePointer(iw) ;
		When* pWhen;
		pWhen = (When*)ptnvoid;
		pWhen->out(offset);
	}

	cout << "  ACTIONS " << endl;
	
	int na = _actions.length();

	for (int ia=0; ia < na; ia++) {
		void* ptnvoid = _actions.gimePointer(ia) ;
		Action* pAction;
		pAction = (Action*)ptnvoid;
		pAction->out(offset);
	}

	return;
}
//------------------------------------------------------- BF April 2000 ---------
void State::outSobj(ofstream& sobj) const
{
	sobj << "*STATE" << endl;
	sobj << _name.getString() << endl;

	
	int nAtt = _pAttributeBlock->_attributes.length();

	sobj << "    " << nAtt << endl;

	for (int i=0; i<nAtt; i++) {
		sobj << _pAttributeBlock->_attributes[i].getString() << endl;
	}

	int nw = _whens.length();

	for (int iw=0; iw < nw; iw++) {
		void* ptnvoid = _whens.gimePointer(iw) ;
		When* pWhen;
		pWhen = (When*)ptnvoid;
		pWhen->outSobj(sobj);
	}


	int na = _actions.length();

	for (int ia=0; ia < na; ia++) {
		void* ptnvoid = _actions.gimePointer(ia) ;
		Action* pAction;
		pAction = (Action*)ptnvoid;
		pAction->outSobj(sobj);
	}

	sobj << "*END_STATE" << endl;
	return;
}
//---------------------------------------------------------------------------
int State::getAttributes(NameVector& attr) const
{
	int nAtt = _pAttributeBlock->_attributes.length();  // number of attribs
	
	for (int i=0; i<nAtt; i++) {
		attr += _pAttributeBlock->_attributes[i];
	}
	return nAtt;
}
//----------------------------------------------------------------------------
bool State::isUndeclared() const
{
	NameVector attribs;
	
	int num = getAttributes(attribs);
	
	if ( num == 0 ) return false;
	
	for (int i=0; i<num; i++)
	{
		if ( attribs[i] == "UNDECLARED_STATE" ) { return true; }
	}
	
	return false;
	
}
//-----------------------------------------------------------------------------
bool State::hasAction(const Name& actionNm) const
{
	int numAct = _actions.length();
	
	void* ptnvoid; Action* pact;
	
	for ( int i=0; i<numAct; i++ )
	{
		ptnvoid = _actions.gimePointer(i);
		pact = (Action*)ptnvoid;
		if ( actionNm == pact->name() ) { return true; }
	}

	return false;
}
//----------------------------------------------------------------------
int State::numActions() const
{
	return _actions.length();
}
//----------------------------------------------------------------------
Action* State:: gimeActionPointer(const int i) const
{
	int noActions = numActions();
	if ( i >= noActions || i<0) { return NULL; }
	
	void* ptnvoid; Action* pact;

	ptnvoid = _actions.gimePointer(i);
	pact = (Action*)ptnvoid;
	return pact;
}
//----------------------------------------------------------------------
Action* State:: gimeActionPointer(const Name& actionNm) const
{
	int noActions = numActions();
	if ( noActions <=0 ) { return NULL; }
	
	void* ptnvoid; Action* pact;

	for ( int i=0; i <noActions; i++ )
	{
		ptnvoid = _actions.gimePointer(i);
		pact = (Action*)ptnvoid;
		if ( pact->name() == actionNm ) { return pact; }
	}
	return NULL;
}
//---------------------------------------------------------------------------
void State::processAttributeBlock(int& istart, int& next_istart, sectionType_t& nextSection)
{
//  create
	_pAttributeBlock = new AttributeBlock();
	assert (_pAttributeBlock != 0);

	SMLUnit* pUnit = _pAttributeBlock;
	Name unitType = "attribute block";
	
	_internalSMLUnits.addRegardless(unitType,pUnit);
	pUnit->setParentUnit(this);
//------------------------------

// collect
	SMLlineType_t nextUnit;

	collectUnit(pUnit,istart,
		    WHEN_LINE, ACTION_LINE,
		    next_istart,nextUnit);
	if ( nextUnit == WHEN_LINE ) { nextSection = SECTION_WHENS; }
	if ( nextUnit == ACTION_LINE ) { nextSection = SECTION_ACTIONS; }
	if ( nextUnit == END_LINE ) { nextSection = SECTION_NULL; }		
// translate
	pUnit->translate();

	return;
} 
//---------------------------------------------------------------------------
void State::createCollectTranslateWhen(int& istart, int& nextUnitStart, SMLlineType_t& nextUnit)
{
//  create
	int whenNo = _whens.length();
	When* pWhen = new When(whenNo);
	assert (pWhen != 0);

	SMLUnit* pUnit = pWhen;
	Name unitType = "When";
	
	_internalSMLUnits.addRegardless(unitType,pUnit);
	pUnit->setParentUnit(this);
	
	Name registeredName = "-";
	_whens.addRegardless(registeredName,pUnit);
//------------------------------

// collect

	collectUnit(pUnit,istart,
	            WHEN_LINE,ACTION_LINE,
		    nextUnitStart,nextUnit);
	
// translate
	pUnit->translate();
	
	SMLlineVector* pGenActionCode = pWhen->getEndStateActionCodePointer();
	if ( pGenActionCode == NULL ) { return; }
	
	int lastNoOfLines = _pSMLcode->length();
	this->acceptLines(*pGenActionCode,0,-1);
	
	if ( nextUnit == END_LINE )
	{
		nextUnit = ACTION_LINE; nextUnitStart = lastNoOfLines;
	}

	return;
} 
//---------------------------------------------------------------------------
void State::createCollectTranslateAction( int& istart, int& nextUnitStart, SMLlineType_t &nextUnit)
{
//  create
	SMLline line; Name name;
	line = (*_pSMLcode)[istart];
	line.lineType(name);

//cout << endl << " createCollectTranslateAction istart " << istart
//      << " First line:" << line << endl;
	
	Action* pAction = new Action(name);
	assert (pAction != 0);

	SMLUnit* pUnit = pAction;
	Name unitType = "Action";
	
	_internalSMLUnits.addRegardless(unitType,pUnit);
	pUnit->setParentUnit(this);
	
	Name registeredName = name;
	int iflag = _actions.add(registeredName,pUnit);
	if (iflag != 1)
	{
		ErrorWarning::printHead("ERROR",line);
		cout << unitType << registeredName 
		<< " has allready been declared" << endl;
		throw FATAL;
	}
//------------------------------

// collect

	collectUnit(pUnit,istart,
	            ACTION_LINE, END_LINE,
		    nextUnitStart,nextUnit);

// translate
	pUnit->translate();

	return;
}
//------------------------------------------------------------------------------
void State::processWhens(int& istart, int& next_istart, sectionType_t& nextSection)
{
//  istart is the position of the first when within the _pSMLcode

	SMLlineType_t nextUnit;
		
	while ( 1==1 )
	{
		createCollectTranslateWhen( istart, next_istart, nextUnit);
	
		if ( nextUnit != WHEN_LINE ) { break; }
		istart = next_istart;
	}
	
	if ( nextUnit == ACTION_LINE ) { nextSection = SECTION_ACTIONS; }
	if ( nextUnit == END_LINE ) { nextSection = SECTION_NULL; }
	return;
}
//----------------------------------------------------------------------------------
void State::processActions(int& istart, int& next_istart, sectionType_t& nextSection)
{

	SMLlineType_t nextUnit;
	
	while ( 1==1 )
	{
		createCollectTranslateAction( istart, next_istart, nextUnit);
	
		if ( nextUnit != ACTION_LINE ) { break; }
		istart = next_istart;
	}

	if ( nextUnit == END_LINE) { nextSection = SECTION_NULL; }
	
	return;
}
