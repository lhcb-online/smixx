// action.hxx: interface for the Action class.
//
//                                                  B. Franek
//                                                 1 June 1999
// Copyright Information:
//      Copyright (C) 1999-2001 CCLRC. All Rights Reserved.
//////////////////////////////////////////////////////////////////////
#ifndef ACTION_HH
#define ACTION_HH

#include "name.hxx"
#include "smlline.hxx"
#include "smlunit.hxx"
#include "actionheadblock.hxx"
#include "inslist.hxx"
#include "ptrvector.hxx"

class Action  : public SMLUnit 
{
public:
	Action(const Name&);

	virtual ~Action();

	virtual void translate();

	Name name() const;
	
	virtual void outSobj(ofstream& sobj) const;

        int getActionParameter(const Name& name, Name& value,
                               Name& typeNm) const;
			       
	int numParameters() const;
	
	Parms* pActionParameters();

/**
   When InsClass object is instantiated, it calls this method supplying its
   pointer and the method will register it in _actionBlocks vector 
*/	
	void registerBlock( InsList* pBlock );

protected :
	
	ActionHeadBlock *_pActionHeadBlock;

	InsList *_pInsList;
	
/**
  vector of pointers to all the blocks that were instantiated during the action
  translation
*/
	PtrVector _actionBlocks; 
private:

/**
   This method will order the pointers in _actionBlocks so that the blocks
   come in increasing level starting with level 0. This is necessary for
   backward compatibility of .sobj files.
*/
	void orderBlockPointers();
/**
   For diagnostic purposes. It prints the source code of blocks in _actionBlocks
*/	
	void printBlocks();				  
};

#endif 
