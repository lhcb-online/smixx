// ifUnit.hxx: interface for the IfUnit class.
//
//                                                  B. Franek
//                                                4 October 1999
//
//////////////////////////////////////////////////////////////////////
#ifndef IFUNIT_HH
#define IFUNIT_HH

#include "smlunit.hxx"
//#include "attributes.hxx"
#include "ifunitheadblock.hxx"
#include "inslist.hxx"
#include "action.hxx"

class IfUnit  : public SMLUnit 
{
public:
	
	IfUnit( Action* pParentAction, InsList* pParentList);

	virtual ~IfUnit();

	virtual void translate() ;

	virtual void outSobj( ofstream& sobj) const;

	void replaceArgs(const NameVector& args)
	{   _pHeadBlock->replaceArgs(args); };

protected :

	IfUnitHeadBlock *_pHeadBlock;

	InsList *_pInsList;
	
	Action* _pParentAction;
	
	InsList* _pParentList;

};

#endif 
